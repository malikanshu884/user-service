package com.mircroservice.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mircroservice.user.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value= " select * from User where user_id=:id", nativeQuery = true)
	User findbyUserId(Long id);
}
