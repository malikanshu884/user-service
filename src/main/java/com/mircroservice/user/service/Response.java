package com.mircroservice.user.service;

import com.mircroservice.user.entity.Department;
import com.mircroservice.user.entity.User;

import lombok.Data;

@Data
public class Response {
	private User user;
	private Department department;

}
