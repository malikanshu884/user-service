package com.mircroservice.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mircroservice.user.entity.Department;
import com.mircroservice.user.entity.User;
import com.mircroservice.user.repo.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RestTemplate resttemplate;

	public User saveUser(User user) {
		return userRepository.save(user);
	}
	
	public Response getDetails(Long id) {
		Response res = new Response();
		User user = userRepository.findbyUserId(id);
		Department dep = resttemplate.getForObject("http://DEPARTMENT-SERVICE/department/"+user.getDepartmentId(), Department.class);
		res.setDepartment(dep);
		res.setUser(user);
		return res;
	}
}
